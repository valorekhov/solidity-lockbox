const Lockbox = artifacts.require("Lockbox");

const returnAddress = '0xD3F81260a44A1df7A7269CF66Abd9c7e4f8CdcD1';
const zeroAddress = '0x0000000000000000000000000000000000000000';

async function createClaimPaymentSignature(forAddress, amount, nonce, contractAddress, signer) {
    const hash = web3.utils.soliditySha3(
        { t: 'address', v: forAddress },
        { t: 'uint256', v: amount },
        { t: 'uint', v: nonce },
        { t: 'address', v: contractAddress }
    );
    return await web3.eth.sign(hash, signer);
}

function confirmPayout(res, account, expectedAmount, nonce){
    let logs = res.logs;
    assert.isNotEmpty(logs);
    assert.equal(logs.length, 1);
    let entry = logs[0];
    assert.isDefined(entry);
    assert.equal(entry.event, 'PayOut');
    assert.isDefined(entry.args);
    assert.equal(entry.args.to, account, `Expecting account ${account}`);
    let expectedAmountDec = expectedAmount.toString(10);
    assert.equal(entry.args.amount.toString(10), expectedAmountDec, `Expecting amount ${expectedAmountDec}`);
    if (nonce != undefined)
        assert.equal(entry.args.nonce.toString(10), nonce.toString(10))
}

contract("Lockbox", accounts => {

    const toBN = web3.utils.toBN;

    const UINT_MAX = toBN('115792089237316195423570985008687907853269984665640564039457584007913129639935');

    const contractOwner = accounts[0];
    const accountTwo = accounts[1];
    const accountThree = accounts[2];
    let instance;

    beforeEach(async () => {
        instance = await Lockbox.new(returnAddress, { from: contractOwner, value: web3.utils.toWei('1', 'ether') });
        assert.isDefined(instance);
        let owner = await instance.owner();
        assert.equal(contractOwner, owner);
    });

    it("should pay on valid requests", async () => {
        instance = await Lockbox.new(returnAddress, { from: contractOwner });
        assert.isDefined(instance);
        const fundingAmount = web3.utils.toWei('1', 'ether');
        await instance.send(fundingAmount, { from: contractOwner });
        let beginningContractBallance = toBN(await web3.eth.getBalance(instance.address));
        assert.equal(fundingAmount, beginningContractBallance);

        let beginningRecepientBallance = toBN(await web3.eth.getBalance(accountTwo));

        const txAmount = toBN(web3.utils.toWei('1', 'gwei'));
        let sig = await createClaimPaymentSignature(accountTwo, txAmount, 1, instance.address, contractOwner);
        let res = await instance.claimPayment(txAmount, 1, sig, { from: accountTwo });
        confirmPayout(res, accountTwo, txAmount, 1);

        let tx = await web3.eth.getTransaction(res.tx);
        let txFee = toBN(tx.gasPrice).mul(toBN(res.receipt.gasUsed));
        //console.log(txFee.toString(10));

        let endingContractBallance = toBN(await web3.eth.getBalance(instance.address));
        let endingRecepientBallance = toBN(await web3.eth.getBalance(accountTwo));

        assert.equal(endingContractBallance.toString(10), beginningContractBallance.sub(txAmount).toString(10));
        assert.equal(endingRecepientBallance.toString(10),
            beginningRecepientBallance.add(txAmount).sub(txFee).toString(10));
    });

    it("should reject requests with a mismatched nonce", async () => {
        const txAmount = web3.utils.toWei('1', 'gwei');
        let sig = await createClaimPaymentSignature(accountTwo, txAmount, 0, instance.address, contractOwner);
        try {
            await instance.claimPayment(txAmount, 1, sig, { from: accountTwo });
            assert.fail('Expecting failure on mismatched nonce');
        } catch(e){
            assert.include(e.message, 'Reason given: Non-owner signature');
        }
    });

    it("should reject requests with a mismatched txAmount", async () => {
        const txAmount = web3.utils.toWei('1', 'gwei');
        let sig = await createClaimPaymentSignature(accountTwo, txAmount, 1, instance.address, contractOwner);
        try {
            await instance.claimPayment(web3.utils.toWei('1', 'ether'), 1, sig, { from: accountTwo });
            assert.fail('Expecting failure on mismatched txAmount');
        } catch(e){
            assert.include(e.message, 'Reason given: Non-owner signature');
        }
    });

    it("should reject requests with a mismatched owner", async () => {
        const txAmount = web3.utils.toWei('1', 'gwei');
        let sig =  await createClaimPaymentSignature(accountTwo, txAmount, 1, instance.address, accountThree);
        try {
            await instance.claimPayment(txAmount, 1, sig, { from: accountTwo });
            assert.fail('Expecting failure on mismatched owner');
        } catch(e){
            assert.include(e.message, 'Reason given: Non-owner signature');
        }
    });

    it("should reject requests with a mismatched sender", async () => {
        const txAmount = web3.utils.toWei('1', 'ether');
        let sig = await createClaimPaymentSignature(accountTwo, txAmount, 1, instance.address, contractOwner);
        try {
            await instance.claimPayment(txAmount, 1, sig, { from: accountThree });
            assert.fail('Expecting failure on mismatched sender');
        } catch(e){
            assert.include(e.message, 'Reason given: Non-owner signature');
        }
    });

    it("should pay out multiple zero-nonce requests", async () => {
        const txAmount = web3.utils.toWei('1', 'wei');
        let sig = await createClaimPaymentSignature(zeroAddress, txAmount, 0, instance.address, contractOwner);

        confirmPayout((await instance.claimPayment(txAmount, 0, sig, { from: accountTwo })), 
            accountTwo,
            txAmount, 0);
        
        confirmPayout(await instance.claimPayment(txAmount, 0, sig, { from: accountThree }), 
            accountThree,
            txAmount,
            0);
    });

    it("should only pay out 1 wei for 0 nonce", async () => {
        let txAmount = web3.utils.toWei('1', 'kwei');
        let sig = await createClaimPaymentSignature(zeroAddress, txAmount, 0, instance.address, contractOwner);
        try {
            await instance.claimPayment(txAmount, 0, sig, { from: accountTwo });
            assert.fail();
        } catch(e){
            assert.include(e.message, 'Reason given: Req. 1 WEI amt for 0 nonce');
        }
    });

    it("should pay out non-zero nonce requests only once", async () => {
        let txAmount = web3.utils.toWei('1', 'kwei');
        let sig = await createClaimPaymentSignature(accountTwo, txAmount, 2, instance.address, contractOwner);

        confirmPayout(await instance.claimPayment(txAmount, 2, sig, { from: accountTwo }),
            accountTwo, txAmount, 2);

        try {
            await instance.claimPayment(txAmount, 2, sig, { from: accountTwo });
            assert.fail();
        } catch(e){
            assert.include(e.message, 'Reason given: Reused nonce');
        }

        try {
            await instance.claimPayment(txAmount, 2, sig, { from: accountThree });
            assert.fail();
        } catch(e){
            assert.include(e.message, 'Reason given: Reused nonce');
        }
    });

    it("should allow owner to return funds, should reject claims on returned nonces", async () => {

        let txAmount = web3.utils.toWei('0.5', 'ether');
       
        confirmPayout(await instance.returnFunds(txAmount, [3,4], { from: contractOwner }),
            returnAddress, txAmount, UINT_MAX);

        // Nonces 3,4 have been marked as used, expect claimPayments to fail
        let sig3 = await createClaimPaymentSignature(accountTwo, txAmount, 3, instance.address, contractOwner);
        try {
            await instance.claimPayment(txAmount, 3, sig3, { from: accountTwo });
            assert.fail();
        } catch(e){
            assert.include(e.message, 'Reason given: Reused nonce');
        }

        let sig4 = await createClaimPaymentSignature(accountThree, txAmount, 4, instance.address, contractOwner);
        try {
            await instance.claimPayment(txAmount, 4, sig4, { from: accountThree });
            assert.fail();
        } catch(e){
            assert.include(e.message, 'Reason given: Reused nonce');
        }
    });

    it("should not allow thirdparties to return funds", async () => {
        let txAmount = web3.utils.toWei('0.5', 'ether');
        try {
            await instance.returnFunds(txAmount, [3], { from: accountTwo });
            assert.fail();
        } catch(e){
            assert.include(e.message, 'Reason given: Non-owner sender');
        }
    });
});