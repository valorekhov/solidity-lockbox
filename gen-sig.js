#!/usr/bin/env node

const w3utils = require('web3-utils');
const eutil = require('ethereumjs-util');
const argv = require('yargs')
    .option('amount', {
        alias: 'a',
        default: 1
    })
    .option('nonce', {
        alias: 'n',
        default: 0
    })
    .option('contractAddress', {
        alias: 'c',
        required: true
    })
    .option('privateKey', {
        alias: 'k'
    })
    .argv;

const hash = w3utils.soliditySha3(
        { t: 'uint256', v: argv.amount },
        { t: 'uint', v: argv.nonce },
        { t: 'address', v: argv.contractAddress }
    );

console.log(hash);

if (argv.privateKey){
    const rsv = eutil.ecsign(eutil.hashPersonalMessage(eutil.toBuffer(hash)), eutil.toBuffer(argv.privateKey));
    console.log(eutil.toRpcSig(rsv.v, rsv.r, rsv.s));
}
